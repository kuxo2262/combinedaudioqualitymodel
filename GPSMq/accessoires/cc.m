function z = cc(x,y)

tmp = corrcoef(x,y);
z = tmp(1,2);

if isnan(z)
    if sum(diff(x(:))) == 0 && sum(diff(y(:))) == 0
        z = 1;
    else
        z = 0;
    end
end