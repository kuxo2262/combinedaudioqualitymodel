function [Ref_l, Ref_r, Test_l, Test_r] =  mreGPSMq_preproc_binaural_v2(x1,x2,def,simdef,simwork,params)
% mreGPSMq_preproc_binaural_v2.m This function includes the front end processing stages
%                    to calculate power and envelope power features
% 
%   Processing stages of GPSMq with binaural extension
%   1. auditory filtering by gammatone filterbank
%   2. envolope extraction by hilbert trafo
%   3. downsampling (optional)
%   5. modulation filterbank
%   6. envelope power calculation by using a multi-resolution filterbank
%
%   INPUT:
%       x1:               Reference signal (clean signal)
%       x2:               Test signal (contains signal manipulations)
%       def:              struct for stimulus related parameters,e.g.,fs
%       simdef:           struct for model related parameters, e.g., filter order,
%                         cut-off frequency 
%       simwork:          struct for parameters applied during simulation, e.g.,
%                         filter coefficients
%       params:           parameters specifying back-end processing
%
%   OUTPUT:
%       Y:            3-dim. output matrix containing multi-res. envelope
%                         power of the corresponding auditory and modulation
%                         filter
%       Y_int:        3-dim. output matrix containing multi-res power
%                         calculated on different time-scales
%       work:         some side informations (e.g. number of auditory
%                         channels), but also some additional signals which
%                         might be useful for analyzation
%       dc2mod:       3-dim output matrix containing power-based values
%                         to weight envelope power SNRs 
%                       
%
% Usage: [out] = mmreGPSMq_preproc_binaural_v2(x,def,simdef,simwork,params)
% author: jan-hendrik.flessner@uni-oldenburg.de
%         thomas.biberger@uni-oldenburg.de
% date:   2018-03-18
% modified: 2019-06-03
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Jan-Hendrik Fle�ner, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------


%% Auditory Filtering (GTFB (with complex-valued output) adopted from Dietz2011 )
channels = length(simwork.analyzer.center_frequencies_hz);
fc=simwork.analyzer.center_frequencies_hz;
% apply auditory filterbank to the outer&middle-ear-filtered
x1_l_gt = gfb_analyzer_process(simwork.analyzer, x1(:,1));
x1_r_gt = gfb_analyzer_process(simwork.analyzer, x1(:,2));
x2_l_gt = gfb_analyzer_process(simwork.analyzer, x2(:,1));
[x2_r_gt, simwork.analyzer] = gfb_analyzer_process(simwork.analyzer, x2(:,2));

%% Hilbert Envelope
x1_l_he=abs(x1_l_gt')/sqrt(2);  % hilbert envelope divided thru sqrt(2) cause power of the hilbert envelope is increased by factor of 2
x1_r_he=abs(x1_r_gt')/sqrt(2);
x2_l_he=abs(x2_l_gt')/sqrt(2);
x2_r_he=abs(x2_r_gt')/sqrt(2);

%% ILD cancelation
for ii = 1:length(fc),
    
    x1_l_rms = rms(x1_l_he(:,ii));
    x2_l_rms = rms(x2_l_he(:,ii));
    x1_r_rms = rms(x1_r_he(:,ii));
    x2_r_rms = rms(x2_r_he(:,ii));
    
    x1diff = x1_l_rms - x1_r_rms;
    x2diff = x2_l_rms - x2_r_rms;
    xLdiff = x1_l_rms - x2_l_rms;
    xRdiff = x1_r_rms - x2_r_rms;    
        
    if abs(x1diff) > 0 || abs(x2diff) > 0
         if sign(xLdiff)*sign(xRdiff) == -1
             if x1_l_rms > x2_l_rms
                 x2_l_he(:,ii) = x2_l_he(:,ii) ./ x2_l_rms .* x1_l_rms;
             elseif x1_l_rms < x2_l_rms
                 x1_l_he(:,ii) = x1_l_he(:,ii) ./ x1_l_rms .* x2_l_rms;
             end
             
             if x1_r_rms > x2_r_rms
                 x2_r_he(:,ii) = x2_r_he(:,ii) ./ x2_r_rms .* x1_r_rms;
             elseif x1_r_rms < x2_r_rms
                 x1_r_he(:,ii) = x1_r_he(:,ii) ./ x1_r_rms .* x2_r_rms;
             end
         else
             if abs(xLdiff) > abs(xRdiff)
                 if abs(x1diff) > abs(x2diff)
                     x1_l_he(:,ii) = x1_l_he(:,ii) ./ x1_l_rms .* x1_r_rms;
                 elseif abs(x1diff) < abs(x2diff)
                     x2_l_he(:,ii) = x2_l_he(:,ii) ./ x2_l_rms .* x2_r_rms;
                 end
             elseif abs(xLdiff) < abs(xRdiff)
                 if abs(x1diff) > abs(x2diff)
                     x1_r_he(:,ii) = x1_r_he(:,ii) ./ x1_r_rms .* x1_l_rms;
                 elseif abs(x1diff) < abs(x2diff)
                     x2_r_he(:,ii) = x2_r_he(:,ii) ./ x2_r_rms .* x2_l_rms;
                 end
             end
         end
    end
end

%% Lowpass 150 HZ

x1_l_lp = filter(simwork.b_env_lp,simwork.a_env_lp,x1_l_he);
x1_r_lp = filter(simwork.b_env_lp,simwork.a_env_lp,x1_r_he);
x2_l_lp = filter(simwork.b_env_lp,simwork.a_env_lp,x2_l_he);
x2_r_lp = filter(simwork.b_env_lp,simwork.a_env_lp,x2_r_he);


%% Resampling (downsampling by factor 4)
if strcmp(simdef.resampling,'on')==1;
    x1_l_lp = resample(x1_l_lp,1,def.downsample_factor);
    x1_r_lp = resample(x1_r_lp,1,def.downsample_factor);
    x2_l_lp = resample(x2_l_lp,1,def.downsample_factor);
    x2_r_lp = resample(x2_r_lp,1,def.downsample_factor);
    
    x1_l_lp = x1_l_lp';
    x1_r_lp = x1_r_lp';
    x2_l_lp = x2_l_lp';
    x2_r_lp = x2_r_lp';
    
    work1l.signal_rect_down = x1_l_lp;
    work1r.signal_rect_down = x1_r_lp;
    work2l.signal_rect_down = x2_l_lp;
    work2r.signal_rect_down = x2_r_lp;
    
elseif strcmp(simdef.resampling,'off')==1;
    % no downsampling
    x1_l_lp = x1_l_lp';
    x1_r_lp = x1_r_lp';
    x2_l_lp = x2_l_lp';
    x2_r_lp = x2_r_lp';
    
    work1l.signal_rect_down = x1_l_lp;
    work1r.signal_rect_down = x1_r_lp;
    work2l.signal_rect_down = x2_l_lp;
    work2r.signal_rect_down = x2_r_lp;
    
else
    disp('Please define if resampling should be used!')
end

%% Modulation Filterbank (AM Processing)

Y1l = zeros(length(work1l.signal_rect_down(1,:)),channels,20);
Y1r = Y1l;
Y2l = Y1l;
Y2r = Y1l;

for ii =1:channels;

    [inf1l_1,y1l]= mfb2_GPSM(work1l.signal_rect_down(ii,:),simdef.mf_mfb2style,def.samplerate_down); 
    [inf1r_1,y1r]= mfb2_GPSM(work1r.signal_rect_down(ii,:),simdef.mf_mfb2style,def.samplerate_down);
    [inf2l_1,y2l]= mfb2_GPSM(work2l.signal_rect_down(ii,:),simdef.mf_mfb2style,def.samplerate_down);
    [inf2r_1,y2r]= mfb2_GPSM(work2r.signal_rect_down(ii,:),simdef.mf_mfb2style,def.samplerate_down);
    
            
        Y1l(:,ii,1:length(inf1l_1)+1)=[y1l(:,1),y1l];
        Y1r(:,ii,1:length(inf1r_1)+1)=[y1r(:,1),y1r];
        Y2l(:,ii,1:length(inf2l_1)+1)=[y2l(:,1),y2l];
        Y2r(:,ii,1:length(inf2r_1)+1)=[y2r(:,1),y2r];

end


    Y1l=Y1l(:,:,1:length(inf1l_1)+1);
    work1l.inf_1=[inf1l_1(1) 1 inf1l_1(2:length(inf1l_1))];
    Y1r=Y1r(:,:,1:length(inf1r_1)+1);
    work1r.inf_1=[inf1r_1(1) 1 inf1r_1(2:length(inf1r_1))];
    Y2l=Y2l(:,:,1:length(inf2l_1)+1);
    work2l.inf_1=[inf2l_1(1) 1 inf2l_1(2:length(inf2l_1))];    
    Y2r=Y2r(:,:,1:length(inf2r_1)+1);
    work2r.inf_1=[inf2r_1(1) 1 inf2r_1(2:length(inf2r_1))];    


Ref_l.Ymod1 = Y1l;
Ref_r.Ymod1 = Y1r;
Test_l.Ymod2 = Y2l;
Test_r.Ymod2 = Y2r;

%% AC-coupled envelope power spectrum (Multi Resolution <=> Orig. Resolution)
[Ref_l.Y1 Ref_l.Y_int1 Ref_l.work1 Ref_l.dc2mod1]=multResGPSM_LpFB_int_SIP_v2(Y1l,fc,work1l.inf_1,def.samplerate_down,work1l,x1_l_lp,params,simdef);
[Ref_r.Y1 Ref_r.Y_int1 Ref_r.work1 Ref_r.dc2mod1]=multResGPSM_LpFB_int_SIP_v2(Y1r,fc,work1r.inf_1,def.samplerate_down,work1r,x1_r_lp,params,simdef);
[Test_l.Y2 Test_l.Y_int2 Test_l.work2 Test_l.dc2mod2]=multResGPSM_LpFB_int_SIP_v2(Y2l,fc,work2l.inf_1,def.samplerate_down,work2l,x2_l_lp,params,simdef);
[Test_r.Y2 Test_r.Y_int2 Test_r.work2 Test_r.dc2mod2]=multResGPSM_LpFB_int_SIP_v2(Y2r,fc,work2r.inf_1,def.samplerate_down,work2r,x2_r_lp,params,simdef);
end