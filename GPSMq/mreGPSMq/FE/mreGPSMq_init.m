function  [def simdef simwork]= mreGPSMq_init(def,simdef)
% mreGPSMq_init.m   some pre-calculations of e.g., filter coefficients that have 
%                   been calculated only one time
% 
% INPUT:        
%       def:        struct for various parameters, e.g.,fs
%    simdef:        struct for model related parameters, e.g., filter order,
%                   cut-off frequency
% 
% OUTPUT:
%       def:        struct for stimulus related parameters,e.g.,fs
%       simdef:     struct for model related parameters, e.g., filter order,
%                   cut-off frequency 
%       simwork:    struct for parameters applied during simulation, e.g.,
%                   filter coefficients
% 
% Usage: [def simdef simwork]= mreGPSMq_init(def,simdef)
% thomas.biberger@uni-oldenburg.de;
% date: 2018-04-12
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------

% Resampling
if strcmp(simdef.resampling,'on')==1;
    def.downsample_factor=8;
    def.samplerate_down=def.samplerate/def.downsample_factor;   % downsample signal by factor 4
elseif strcmp(simdef.resampling,'off')==1;
    def.samplerate_down=def.samplerate;     % no downsampling
else
    disp('Please define if resampling should be used!')
end

% parameter (CFs etc.) of the GTFB 
simwork.analyzer = gfb_analyzer_new_tb(def.samplerate, simdef.gt_MinCF, simdef.gt_align, simdef.gt_MaxCF,...
                            simdef.gt_Dens);

% iso hearing threshold
vsDesF=0:def.samplerate/def.intervallen:floor((def.intervallen/2))*def.samplerate/def.intervallen;   % NEW: freq vector needed for isothr

[simwork.vIsoThrDB, simwork.vsF] = iso389_7(vsDesF);

% 1st order butterworth-filter for 150Hz-envelope-lp
simdef.env_lp_order=1;
simdef.env_lp_cutoff=150;
simdef.env_lp_cutoff_norm=simdef.env_lp_cutoff/(def.samplerate/2);
[simwork.b_env_lp,simwork.a_env_lp] = butter(simdef.env_lp_order,simdef.env_lp_cutoff_norm,'low');

end
