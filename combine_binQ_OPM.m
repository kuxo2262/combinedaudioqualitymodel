function [ obj_meas ] = combine_binQ_OPM(obj_meas_mon, obj_meas_bin)
% combine_binQ_OPM.m  Function combines the monaural quality measure (OPM) and the binaural 
% quality measure(binQ) to calculate an overall audio quality measure.
% 
% INPUT:        
%  obj_meas_mon:        monaural output measure (OPM) of GPSMq (Biberger et al., 2018)
%  obj_meas_bin:        binaural output measure (binQ) of BAM-Q (Fle�ner et al., 2017)
% 
% OUTPUT:
%  obj_meas_bin:        struct for stimulus related parameters,e.g.,fs
% 
% Usage: [ obj_meas ] = combine_binQ_OPM(obj_meas_mon, obj_meas_bin)
% thomas.biberger@uni-oldenburg.de;
% date: 2019-09-09
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Jan-Hendrik Fle�ner, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------

obj_meas_mon(obj_meas_mon>100)=100; % limitation of upper boundary -> this is only relevant if reference vs. reference is tested
obj_meas_bin(obj_meas_bin>100)=100; % limitation of upper boundary -> this is only relevant if reference vs. reference is tested
mon_tmp=0.0528.*obj_meas_mon; % for values see Table II in Fle�ner et al. (2019)
bin_tmp=0.0078*obj_meas_bin;  % for values see Table II in Fle�ner et al. (2019)
obj_meas=min(log10(mon_tmp),bin_tmp);

end

